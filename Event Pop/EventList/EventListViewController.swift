//
//  EventListViewController.swift
//  Event Pop
//
//  Created by Nutthapoom on 19/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import UIKit
import Imaginary

//struct Events:Decodable {
//    let id: Int?
//    let title: String?
//    let cover: String?
//    let start_at: String?
//    let end_at: String?
//    let location: String?
//
////    init(json: [String: Any]) {
////        id = json["id"] as? Int ?? 1
////        title = json["title"] as? String ?? ""
////        imgURl = json["cover"] as? String ?? ""
////        dateStart = json["start_at"] as? String ?? ""
////        dateStop = json["end_at"] as? String ?? ""
////        location = json["location"] as? String ?? ""
////    }
//}

class EventListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var events = [Events]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerTableViewCell()
        self.loadJsonFile()
        
        tableView.delegate = self
        tableView.dataSource = self

    }
    
    func registerTableViewCell() {
        let nibEventListView = UINib(nibName: "EventListTableViewCell", bundle: nil)
        self.tableView.register(nibEventListView, forCellReuseIdentifier: EventListTableViewCell.reusableIdentifier)
    }
    
    func loadJsonFile() {
        
        guard let path = Bundle.main.path(forResource: "events", ofType: "json") else { return }
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            print(data)
            
//            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
//            print(json)
            
            self.events = try JSONDecoder().decode([Events].self, from: data)
            
//            guard let array = json as? [Any] else { return }
            
//            for event in events {
//                print(event.cover!)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
//                guard let eventDict = event as? [String: Any] else {return}
//                guard let id = eventDict["id"] as? Int else {print("not an int"); return}
//                guard let title = eventDict["title"] as? String else {return}
//                guard let imgURl = eventDict["cover"] as? String else {return}
//                guard let dateStart = eventDict["start_at"] as? String else {return}
//                guard let dateStop = eventDict["end_at"] as? String else {return}
//                guard let location = eventDict["location"] as? String else {return}
//                print(id)
//                print(title)
//                print(dateStart + dateStop)
//                print(location)
//                print(" ")
//            }
        }
        catch {
            print(error)
        }
    }
    
    func convertUTCDateToLocalDate(dateToConvert:String) -> String {
        
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let convertedDate = format.date(from: dateToConvert)
        format.timeZone = TimeZone.current
        format.dateFormat = "dd MMMM yyyy HH:mm"
        let localDateStr = format.string(from: convertedDate!)
        return localDateStr
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EventListCell", for: indexPath) as? EventListTableViewCell else { fatalError("The dequeued cell is not an instance of TableViewCell.") }
        let eventData = events[indexPath.row]
        let urlStr = "http://www.\(eventData.cover!)"
        let imgURL = URL(string: urlStr)
        cell.imgEvent?.setImage(url: imgURL!)
        cell.lblEventTitle?.text = eventData.title!
        cell.lblEventDate?.text = "\(convertUTCDateToLocalDate(dateToConvert: eventData.start_at!)) - \(convertUTCDateToLocalDate(dateToConvert:eventData.end_at!))"
        cell.lblLocation?.text = eventData.location!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail:EventDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetail") as! EventDetailViewController
        let pagNotFound:PageNotFoundViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageNotFound") as! PageNotFoundViewController
        
        let eventData = events[indexPath.row]
        let eventId = eventData.id
        
        if eventId! <= 5 {
            detail.eventId = eventData.id
            self.navigationController?.pushViewController(detail, animated: true)
        } else {
            print("Event Id : ",  eventId!)
            self.navigationController?.pushViewController(pagNotFound, animated: true)
        }
//        detail.strregion = "Region :\(arrdata[indexPath.row].region)"
//        detail.strsubregion = "SubRegion :\(arrdata[indexPath.row].subregion)"
//        detail.stralpha3 = "Alpha3code :\(arrdata[indexPath.row].alpha3Code)"
//        detail.stralpha2 = "Alpha2code :\(arrdata[indexPath.row].alpha2Code)"
//        self.navigationController?.pushViewController(detail, animated: true)
    }

}
