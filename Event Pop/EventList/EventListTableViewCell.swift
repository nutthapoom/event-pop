//
//  EventListTableViewCell.swift
//  Event Pop
//
//  Created by Nutthapoom on 19/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {
    
    static let reusableIdentifier = "EventListCell"

    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
