//
//  Event.swift
//  Event Pop
//
//  Created by Nutthapoom on 20/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import Foundation

struct Details: Decodable {
    let event: Detail
}

struct Events: Decodable {
    let id: Int?
    let title: String?
    let cover: String?
    let start_at: String?
    let end_at: String?
    let location: String?
    
    //    init(json: [String: Any]) {
    //        id = json["id"] as? Int ?? 1
    //        title = json["title"] as? String ?? ""
    //        imgURl = json["cover"] as? String ?? ""
    //        dateStart = json["start_at"] as? String ?? ""
    //        dateStop = json["end_at"] as? String ?? ""
    //        location = json["location"] as? String ?? ""
    //    }
}

struct Detail: Decodable {
    let cover: String?
    let description: String?
    let title: String?
    let start_at: String?
    let end_at: String?
    let location: String?
    let ticket_types: [String]?
    
    
    //    init(json: [String: Any]) {
    //        id = json["id"] as? Int ?? 1
    //        title = json["title"] as? String ?? ""
    //        imgURl = json["cover"] as? String ?? ""
    //        dateStart = json["start_at"] as? String ?? ""
    //        dateStop = json["end_at"] as? String ?? ""
    //        location = json["location"] as? String ?? ""
    //    }
}

