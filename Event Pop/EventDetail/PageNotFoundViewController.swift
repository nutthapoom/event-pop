//
//  PageNotFoundViewController.swift
//  Event Pop
//
//  Created by Nutthapoom on 21/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import UIKit

class PageNotFoundViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backToEventListTap(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }

}
