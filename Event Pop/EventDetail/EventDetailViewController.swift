//
//  EventDetailViewController.swift
//  Event Pop
//
//  Created by Nutthapoom on 20/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import UIKit
import Imaginary

class EventDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateStart: UILabel!
    @IBOutlet weak var lblDateEnd: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var tableViewHeightLayout: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var details = Detail.self
    var ticketData = [Ticket]()
    var eventId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerTableViewCell()
        self.loadJsonFile()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableViewHeightLayout.constant = tableView.contentSize.height
    }
    
    func registerTableViewCell() {
        let nibEventListView = UINib(nibName: "TicketPriceTableViewCell", bundle: nil)
        self.tableView.register(nibEventListView, forCellReuseIdentifier: TicketPriceTableViewCell.reusableIdentifier)
    }
    
    func loadJsonFile() {
        
        var jsonPath: String?
        
        switch eventId {
        case 1:
               jsonPath = "event_1"
        case 2:
            jsonPath = "event_2"
        case 3:
            jsonPath = "event_3"
        case 4:
            jsonPath = "event_4"
        case 5:
            jsonPath = "event_5"
        default:
            jsonPath = ""
        }
        
        
        guard let path = Bundle.main.path(forResource: jsonPath, ofType: "json") else { return }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            let jsonData = try JSONDecoder().decode(Event.self, from: data)
            self.ticketData = jsonData.event.ticket_types
            
            let event = jsonData.event
            
            let attributedString = NSMutableAttributedString(string: event.description)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 4
            paragraphStyle.alignment = .justified
            paragraphStyle.paragraphSpacingBefore = 50
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

            
            DispatchQueue.main.async {
                let urlStr = "http://www.\(event.cover)"
                let imgURL = URL(string: urlStr)
                
                self.imgEvent.setImage(url: imgURL!)
                self.lblTitle.text! = event.title
                self.lblDateStart.text! = self.convertUTCDateToLocalDate(dateToConvert: event.start_at)
                self.lblDateEnd.text = self.convertUTCDateToLocalDate(dateToConvert: event.end_at)
                self.lblLocation.text = event.location
                self.lblDescription.attributedText = attributedString
                self.tableView.reloadData()
            }
            
        }
        catch {
            print(error)
        }
    }
    
    func convertUTCDateToLocalDate(dateToConvert:String) -> String {
        
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let convertedDate = format.date(from: dateToConvert)
        format.timeZone = TimeZone.current
        format.dateFormat = "dd MMMM yyyy HH:mm"
        let localDateStr = format.string(from: convertedDate!)
        return localDateStr
    }
    
    @IBAction func closeEventDetail(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.ticketData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TicketCell", for: indexPath) as? TicketPriceTableViewCell else { fatalError("The dequeued cell is not an instance of TableViewCell.") }
        
        let ticket = ticketData[indexPath.row]
        
        cell.lblTicketType?.text = ticket.name
        cell.lblTicketPrice?.text = String(ticket.price) + " " + "THB."
        return cell
    }

}

//TODO :- Big
//let dateFormat = DateFormatter()
//dateFormat.locale = Locale(identifier: "en_US_POSIX")
//dateFormat.dateFormat = "yyyy-MM-dd"
//let decoder = JSONDecoder()
//decoder.dateDecodingStrategy = .formatted(dateFormat)
//let rates = try decoder.decode([LenederRates].self, from: data)
//print(rates)
