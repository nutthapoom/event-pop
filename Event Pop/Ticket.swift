//
//  Ticket.swift
//  Event Pop
//
//  Created by Nutthapoom on 25/12/2561 BE.
//  Copyright © 2561 Nutthapoom. All rights reserved.
//

import Foundation

struct Event: Decodable {
    var event: EventDetail
}
struct EventDetail: Decodable {
    var id: Int
    var title: String
    var cover: String
    var start_at: String
    var end_at: String
    var location: String
    var description: String
    var ticket_types: [Ticket]
}

struct Ticket: Decodable {
    var id: Int
    var name: String
    var price: Float
}

